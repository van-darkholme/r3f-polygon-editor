import React from 'react';

import Polygon from './Polygon'

import './home.css'
import { Canvas, useFrame } from '@react-three/fiber'
import { useMemo, useState } from 'react/cjs/react.development'

const Editor = (props) => {

    return (
        <div className="card">
            <div className="card-header">
            </div>
            <div className="card-content">
                <div className="cnv">
                    <Canvas orthographic={true} camera={{ far: 3000, near: -3000 }}>
                        <group>
                            {props.meshes.map(function (mesh, index) {
                                return <Polygon 
                                setHoveredIdx={props.setHoveredIdx}
                                setActiveIdx={props.setActiveIdx}
                                index={index}
                                key={index}
                                isActive={props.activeIdx == index}
                                points={mesh.points}
                                position={[0, 0, 0]}
                                width={props.width}
                                height={props.height}
                                ctrlDown={props.ctrlDown}
                                editShapeMode={props.editShapeMode} />
                            })}
                        </group>
                    </Canvas>
                </div>
            </div>
        </div>

    )
}

export default Editor;
