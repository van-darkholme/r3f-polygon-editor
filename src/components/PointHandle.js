import React, { useRef, useState, useMemo, useLayoutEffect } from 'react'
import { Canvas, useFrame } from '@react-three/fiber'
import * as THREE from 'three'

function PointHandle(props) {
    // This reference will give us direct access to the THREE.Mesh object
    const ref = useRef()
    var pos = new THREE.Vector3(props.position.x, props.position.y, 3)
    const movePoint = props.move
    const [active, setActive] = useState(false)
    
    const onPointerDown = () => {
        if(!props.ctrlDown) {
            setActive(true)
        } else {
            console.log('locked, ctrl')
        }
    }

    const onPointerUp = () => {
        if(!props.ctrlDown){
            setActive(false)
        }else {
            console.log('locked, ctrl')
        }
    }

    useMemo(() => {
        if(active && !props.ctrlDown) {
            movePoint(props.idx, props.mousePos[0], props.mousePos[1])
        }
    }, [props.mousePos])

    const onClickHandle = (e) => {
        if(props.ctrlDown) {
            props.delete(props.idx)
        }
    }

    return (
        <mesh
            onClick={onClickHandle}
            onPointerDown={onPointerDown}
            onPointerUp={onPointerUp}
            position={pos}
            ref={ref}
            scale={1}>
            <planeGeometry args={[15, 15]} />
            <meshBasicMaterial color={active? 'green' : 'blue'} transparent={true} opacity={0.4} />
        </mesh>

    )
}

export default PointHandle