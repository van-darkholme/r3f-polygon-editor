import React, { useRef, useState, useMemo, useLayoutEffect } from 'react'
import { Canvas, useFrame } from '@react-three/fiber'
import * as THREE from 'three'

function Cursor(props) {
    // This reference will give us direct access to the THREE.Mesh object
    const ref = useRef()
    const [mousePos, setMousePos] = useState(new THREE.Vector3(0, 0, 0))
    useFrame(({ mouse, camera }) => {
        setMousePos([mouse.x * 250, mouse.y * 250, 0])
    })
    console.log('cursooo')
    return (
        <mesh
            position={mousePos}
            ref={ref}
            scale={1}>
            <circleGeometry args={[20, 32]} />
            <meshBasicMaterial color={'blue'} />
        </mesh>

    )
}

export default Cursor