import React, { useEffect } from 'react';
import { useState } from 'react/cjs/react.development';

const EditorMeshLine = (props) => {
    const [className, setClassName] = useState('column')
    useEffect(() => {
        let classes = ['columns']
        if(props.hoveredIdx == props.index) {
            classes.push('hovered')
        }
        if(props.activeIdx == props.index) {
            classes.push('active')
        }
        setClassName(classes.join(' '))
    }, [props.hoveredIdx, props.activeIdx])

    const selectMesh = () => {
        props.setActiveIdx(props.index)
    }

    return (
        <div className={className}>
            <div className="column">
                <input onClick={selectMesh} onChange={props.renameMesh} data-idx={props.index} className="input" defaultValue={props.mesh.name}></input>
            </div>
            <div onClick={props.deleteMesh} className="column"><button className="button is-danger">X</button></div>

        </div>
    )
}

export default EditorMeshLine
