import React, { useRef, useState, useMemo, useLayoutEffect } from 'react'
import { Canvas, useFrame, useThree } from '@react-three/fiber'
import * as THREE from 'three'
import { dotLineLength, lineLength } from '../utils/dotlinelength'
import PointHandle from './PointHandle'

function Polygon(props) {
    // This reference will give us direct access to the THREE.Mesh object
    const ref = useRef()

    // Set up state for the hovered and active state
    const [hovered, setHover] = useState(false)

    const [points, setPoints] = useState(props.points)

    const [mousePos, setMousePos] = useState(new THREE.Vector3(-9000, -9000, 0))
    const [addPointCursorVisible, setAddPointCursorVisible] = useState(false)
    const [newPointAt, setNewPointAt] = useState(undefined)

    const state = useThree()

    useFrame(({ mouse, camera }) => {
        let x = mouse.x * props.width / 2
        let y = mouse.y * props.height / 2
        if (x === 0 && y === 0) {
            return
        }
        setMousePos([x, y, 0])
        let insertAt = checkInsertPointAtLine(x, y)
        if (insertAt !== undefined) {
            setAddPointCursorVisible(true)
            setNewPointAt(insertAt)
        } else {
            setNewPointAt(undefined)
            setAddPointCursorVisible(false)
        }
    })

    const getPoint = (idx) => {
        let ln = points.length
        if (idx < 0) {
            return points[ln + idx]
        }
        return points[idx]
    }

    const insertPoint = (idx, x, y) => {
        var copy = [...points]
        copy.splice(idx, 0, new THREE.Vector2(Math.round(x), Math.round(y)))
        setPoints(copy)
    }

    const movePoint = (idx, x, y) => {
        if (props.editShapeMode != 'move_points') {
            return
        }
        var copy = [...points]
        copy[idx] = new THREE.Vector2(x, y)
        setPoints(copy)
    }

    const insertPointOnClick = () => {
        if (props.editShapeMode != 'edit_points') {
            return
        }
        if (newPointAt === undefined) {
            return
        }

        for (var i = 0; i < points.length; i++) {
            let pt = points[i]
            let llen = lineLength(mousePos[0], mousePos[1], pt.x, pt.y)
            if (llen < 16) {
                return
            }
        }

        insertPoint(newPointAt, mousePos[0], mousePos[1])
    }

    const checkInsertPointAtLine = (x, y) => {
        for (var i = 0; i < points.length; i += 1) {
            let lineDis = dotLineLength(
                x, y,
                getPoint(i).x, getPoint(i).y,
                getPoint(i - 1).x, getPoint(i - 1).y,
                true
            );


            if (lineDis < 6) {
                return i
            }
        }
    }

    const deletePoint = (idx) => {
        var copy = [...points]
        copy.splice(idx, 1);
        if (copy.length > 2) {
            setPoints(copy)
        }
    }

    const shape = useMemo(() => {
        return new THREE.Shape(points)
    }, [points])

    const [dragMousePosStart, setDragMousePosStart] = useState(null)

    const dragStart = (e) => {
        setDragMousePosStart([state.mouse.x * props.width / 2, state.mouse.y * props.width / 2])
    }

    const dragEnd = (e) => {
        props.setHoveredIdx(null)
        setDragMousePosStart(null)
    }
    var cnt = 0
    const drag = (e) => {
        if(props.editShapeMode != 'move_mesh' || dragMousePosStart === null) {
            return
        }
        let mpos = [state.mouse.x * props.width / 2, state.mouse.y * props.width / 2]
        let deltaX = dragMousePosStart[0] - mpos[0]
        let deltaY = dragMousePosStart[1] - mpos[1]
        var copy = [...points]
        copy.forEach((v, i) => {
            copy[i].x -= deltaX
            copy[i].y -= deltaY
        })
        setDragMousePosStart(mpos)
        cnt += 1
        if(cnt > 10) {
            console.log(deltaX, deltaY)
            cnt = 0
        }
        setPoints(copy)
    }

    const setActive = (e) => {
        props.setActiveIdx(props.index)
    }

    const onHover = () => {
        props.setHoveredIdx(props.index)
    }

    return (
        <group>
            <mesh
                {...props}
                ref={ref}
                scale={1}
                onDoubleClick={(event) => setActive(event)}
                onPointerDown={(event) => dragStart()}
                onPointerUp={(event) => dragEnd()}
                onPointerMove={(event) => drag()}
                onPointerOver={(event) => onHover()}
                onPointerOut={(event) => dragEnd()}>
                <shapeGeometry args={[shape]} />
                <meshBasicMaterial color={props.isActive ? 'orange' : 'green'} transparent={true} opacity={0.7} />
            </mesh>
            {props.isActive && props.editShapeMode == 'edit_points' &&
                <mesh
                    onClick={(event) => insertPointOnClick()}
                    visible={addPointCursorVisible}
                    position={mousePos}
                    ref={ref}
                    scale={1}>
                    <circleGeometry args={[5, 32]} />
                    <meshBasicMaterial color={'green'} opacity={0.5} />
                </mesh>
            }
            {props.isActive && (props.editShapeMode == 'edit_points' || props.editShapeMode == 'move_points') &&
                <group>
                    {points.map(function (pos, index) {
                        return <PointHandle delete={deletePoint} ctrlDown={props.ctrlDown} position={pos} key={index} idx={index} mousePos={mousePos} move={movePoint} />
                    })}
                </group>
            }
        </group>
    )
}

export default Polygon