import React, { useEffect } from 'react';
import './home.css'
import * as THREE from 'three'
import MoveMeshIcon from './svg/move-mesh.svg'
import MovePointIcon from './svg/move-point.svg'
import AddPointIcon from './svg/add-point.svg'
import RemovePointIcon from './svg/remove-point.svg'
import Editor from './Editor';
import { useMemo, useState } from 'react/cjs/react.development'
import EditorMeshLine from './EditorMeshLine'

const Home = () => {
    const [meshes, setMeshes] = useState([])
    const [activeIdx, setActiveIdx] = useState(0)
    const [hoveredIdx, setHoveredIdx] = useState(null)

    const [editShapeMode, setEditShapeMode] = useState('move_mesh')

    const setMovePoints = () => {
        setEditShapeMode('move_points')
    }

    const setEditPoints = () => {
        setEditShapeMode('edit_points')
    }
    
    const setMoveMesh = () => {
        setEditShapeMode('move_mesh')
    }

    const setActiveMesh = (actIdx) => {
        console.log("make active:", actIdx)
        var copy = [...meshes]
        copy.forEach((v, idx) => {
            v.active = (idx == actIdx)
        })
        setMeshes(copy)
    }

    const width = 900
    const height = 900
    const defaultMesh = {
        name: 'new area',
        points: [
        new THREE.Vector2(0, 0),
        new THREE.Vector2(50, 50),
        new THREE.Vector2(100, 0),
        new THREE.Vector2(75, -50),
        new THREE.Vector2(25, -50),
    ]}

    const handleAddPolygonClick = () => {
        setMeshes([...meshes, defaultMesh])
    }

    const [ctrlDown, setCtrlDown] = useState(false)

    const keyDownHandler = (e) => {
        if (e.ctrlKey) {
            setCtrlDown(true)
        }
    }


    const keyUpHandler = (e) => {
        if (e.keyCode == 17) {
            console.log("KEYUP ")
            setCtrlDown(false)
        }
    }
    useEffect(() => {
        document.removeEventListener('keydown', keyDownHandler)
        document.removeEventListener('keyup', keyUpHandler)
        document.addEventListener('keydown', keyDownHandler)
        document.addEventListener('keyup', keyUpHandler)
        console.log("DUPA")    
    }, [])

    useMemo(() => {
        const body = document.body
        body.classList.remove('add')
        body.classList.remove('remove')
        body.classList.remove('move')
        if (editShapeMode && ctrlDown) {
            body.classList.add('remove')
        }
        if (editShapeMode && !ctrlDown) {
            body.classList.add('add')
        }
        if (!editShapeMode) {
            body.classList.add('move')
        }
    }, [ctrlDown, editShapeMode])



    const selectMesh = (e) => {
        const actIdx = e.target.dataset.idx
        setActiveMesh(actIdx)
    }

    const renameMesh = (e) => {
        const target = e.target
        const actIdx = target.dataset.idx
        var copy = [...meshes]        
        copy[actIdx].name = e.target.value
        setMeshes(copy)
    }

    const deleteMesh = (e) => {
        const target = e.target
        const idx = target.dataset.idx
        var copy = [...meshes]        
        copy.splice(idx, 1);
        setMeshes(copy)
    }

    return (
        <div className="container">
            <nav className="nav"></nav>
            <div className="columns">
                <div className="column">
                    <Editor setActiveIdx={setActiveIdx} activeIdx={activeIdx} setHoveredIdx={setHoveredIdx} meshes={meshes} editShapeMode={editShapeMode} ctrlDown={ctrlDown} width={width} height={height} />
                </div>
                <div className="column">
                    <button className={editShapeMode == "move_mesh" ? "button is-success" : "button"} onClick={setMoveMesh}><MoveMeshIcon width={32} height={32}></MoveMeshIcon></button>
                    <button className={editShapeMode == "move_points" ? "button is-success" : "button"} onClick={setMovePoints}>
                        {!ctrlDown && <MovePointIcon width={32} height={32} />}
                        {ctrlDown && <RemovePointIcon width={32} height={32} />}
                    </button>
                    <button className={editShapeMode == "edit_points" ? "button is-success" : "button"} onClick={setEditPoints}>
                        {!ctrlDown && <AddPointIcon width={32} height={32} />}
                        {ctrlDown && <RemovePointIcon width={32} height={32} />}
                    </button>

                    <div className="card">
                        <div className="card-header">
                            <nav className="navbar">
                                <div className="navbar-item">
                                    Meshes
                                </div>
                                <div className="navbar-item">
                                    <button className={"button"} onClick={handleAddPolygonClick}><i className="fas fa-add"></i> Add</button>
                                </div>
                            </nav>
                        </div>
                        <div className="card-content">
                                {meshes.map(function (mesh, index) {
                                    return <EditorMeshLine activeIdx={activeIdx} hoveredIdx={hoveredIdx} setActiveIdx={setActiveIdx} deleteMesh={deleteMesh} selectMesh={selectMesh} renameMesh={renameMesh} key={index} index={index} mesh={mesh} />
                                })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;
